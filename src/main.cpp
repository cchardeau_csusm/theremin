#include "mbed.h"
#include "pindef.h"
#include "ultrasonic.h"

#define VOLUME_THRESHOLD 400.0
#define PITCH_THRESHOLD 400.0

PwmOut			speaker(Dout3);
Serial			pc(USBTX, USBRX);
DigitalOut	led(LED_b);

void				volumeDist(int distance);
void				pitchDist(int distance);

ultrasonic	volumeSensor(D5, D4, .1, 1, &volumeDist);
ultrasonic	pitchSensor(D7, D6, .1, 1, &pitchDist);

void				volumeDist(int distance)
{
		pc.printf("[Volume] Distance changed to %d mm\r\n", distance);
		if (distance <= VOLUME_THRESHOLD) {
			led = 1;
			speaker = 1 - (distance / VOLUME_THRESHOLD);
		} else {
			led = 0;
			speaker = 0;
		}
}
 
void				pitchDist(int distance)
{
		pc.printf("[Pitch] Distance changed to %d mm\r\n\n", distance);
		if (distance <= PITCH_THRESHOLD) {
			led = 1;
			speaker.period(1.0 / (200 + (19800 * (distance / PITCH_THRESHOLD))));
		} else {
			led = 0;
		}
}

int main()
{
		pc.printf("\033[2J");
		pc.printf("START PROGRAM\n");
		led = 0;
	
		volumeSensor.startUpdates();
		pitchSensor.startUpdates();
	
    while(1) {
			volumeSensor.checkDistance();
			pitchSensor.checkDistance();
    }
}